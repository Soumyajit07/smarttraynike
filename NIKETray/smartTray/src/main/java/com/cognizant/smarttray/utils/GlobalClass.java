package com.cognizant.smarttray.utils;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.android.volley.RequestQueue;

import java.util.ArrayList;

/**
 * Created by 452781 on 9/26/2016.
 */
public class GlobalClass extends MultiDexApplication {
    public static final String TAG = GlobalClass.class.getSimpleName();
    private RequestQueue mRequestQueue;
    private static Context sContext;
    private static GlobalClass mInstance;

    ArrayList<TrayData> trays = new ArrayList<TrayData>();

    @Override
    public void onCreate() {
        super.onCreate();
        GlobalClass.setAppContext(getApplicationContext());
        mInstance = this;
    }

    public static void setAppContext(Context context) {
        Log.d(TAG, String.format("setAppContext: %s", context));
        sContext = context;
    }

    public static Context getAppContext() {
        return sContext;
    }

    public static synchronized GlobalClass getInstance() {
        return mInstance;
    }

    public ArrayList<TrayData> getTrays() {

        return trays;
    }

    public void setTrays(ArrayList<TrayData> trays) {
        this.trays = trays;

    }


}
