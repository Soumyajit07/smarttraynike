package com.cognizant.smarttray.model;

/**
 * Created by 452781 on 10/4/2016.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class IRSensorConfiguration {

    @SerializedName("SensorId")
    @Expose
    private String sensorId;
    @SerializedName("ProductId")
    @Expose
    private String productId;
    @SerializedName("IRThreshold")
    @Expose
    private Integer iRThreshold;

    /**
     * @return The sensorId
     */
    public String getSensorId() {
        return sensorId;
    }

    /**
     * @param sensorId The SensorId
     */
    public void setSensorId(String sensorId) {
        this.sensorId = sensorId;
    }

    /**
     * @return The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId The ProductId
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return The iRThreshold
     */
    public Integer getIRThreshold() {
        return iRThreshold;
    }

    /**
     * @param iRThreshold The IRThreshold
     */
    public void setIRThreshold(Integer iRThreshold) {
        this.iRThreshold = iRThreshold;
    }

}



