package com.cognizant.smarttray.network;
import android.util.Log;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

//
//import org.apache.http.entity.ContentType;
//import org.apache.http.entity.mime.MultipartEntity;
//import org.apache.http.entity.mime.MultipartEntityBuilder;

//import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


/**
 * Volley adapter for JSON requests that will be parsed into Java objects by
 * Gson.
 */
public class GsonRequest<T> extends Request<T> {

    private final Gson gson = new Gson();
    private final Class<T> clazz;
    private Map<String, String> headers;
    private final Listener<T> listener;
    private JSONObject parameters = null;

    private String stringparameters = null;
    private JSONArray arrayparameters = null;




    /**
     * Make a GET request and return a parsed object from JSON.
     * @param url     URL of the request to make
     * @param clazz   Relevant class object, for Gson's reflection
     */
    public GsonRequest(int method, String url, Class<T> clazz,
                       Listener<T> listener,
                       ErrorListener errorListener) {
        super(method, url, errorListener);
        this.clazz = clazz;
//        this.headers = headers;
        this.listener = listener;
    }

    public GsonRequest(int method, String url, Class<T> clazz,
                       Map<String, String> headers, JSONObject parameters,
                       Listener<T> listener, ErrorListener errorListener) {
        this(method, url, clazz, listener, errorListener);
        this.parameters = parameters;
    }

    public GsonRequest(int method, String url, Class<T> clazz,
                       Map<String, String> headers, String stringparameters,
                       Listener<T> listener, ErrorListener errorListener) {
        this(method, url, clazz, listener, errorListener);
        this.stringparameters = stringparameters;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {

        return headers != null ? headers : super.getHeaders();
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }


    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            if (parameters != null) {
                return parameters.toString().getBytes(getParamsEncoding());
            }

            if (stringparameters != null) {
                return stringparameters.getBytes(getParamsEncoding());
            }

            if (arrayparameters != null) {
                return arrayparameters.toString().getBytes(getParamsEncoding());
            }




        } catch (UnsupportedEncodingException e) {

            Log.d("ERROR", e.toString());

        }
        return null;
    }

    @Override
    protected void deliverResponse(T response) {
             listener.onResponse(response);

    }

    @SuppressWarnings("unchecked")
    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        Log.i("resp", response.statusCode + "resp2" + response.data.toString());


        try {
            String json = new String(response.data);
            Log.e("-----___----___",json);

            Log.i("Network SensorResponse", json.trim());

            JsonParser parser = new JsonParser();
            JsonElement element = parser.parse(json);

            Map<String, String> params = new HashMap<>();
            params.put("response", json);

            if(!element.isJsonObject())
            {
                JSONObject myobject = new JSONObject(params);
                element = parser.parse(myobject.toString());
            }

            if (element.isJsonObject()) {
                return Response.success(gson.fromJson(json, clazz),
                        HttpHeaderParser.parseCacheHeaders(response));
            } else {

                Log.i("parse error", "error");

                return (Response<T>) Response.success(json,
                        HttpHeaderParser.parseCacheHeaders(response));

            }

        } catch (JsonSyntaxException e) {
            Log.i("json parse error", "json error");
            Log.e("error", e.toString());
            return Response.error(new ParseError(e));
        }


    }

    private static final int LOGCAT_MAX_LENGTH = 3950;



}