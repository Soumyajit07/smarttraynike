package com.cognizant.smarttray.utils;

import android.text.Html;
import android.util.Log;

import com.cognizant.smarttray.activity.SingleTrayActivity;
import com.cognizant.smarttray.model.Response;
import com.cognizant.smarttray.model.SingleTraySlot;
import com.cognizant.smarttray.model.TrayDetailsData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 452781 on 9/29/2016.
 */
public class TrayDataPopulate {

    static List<TrayDetailsData> detailsdata;

    static String irCountString;
    static String totalIrCountString;
    static String uvCountString;
    static String totalUVCountString;
    static String weightCountString;
    static String totalWeightCountString;
    static String irName;
    static String uvName;
    static String wtName;

    public static String inStoreColourStringIR = "green";
    public static String inStoreColourStringUV = "green";
    public static String weightColourString = "green";

    public static String poidWT = "91C1808D-5C9D-465D-82B8-3F1B45F0FB4F";
    public static String poidIR = "5B3894F1-26AA-41E6-B2F2-590AEDF16851";

    public static String warehouseColourString = "green";
    public static String warehouseColourStringUV = "green";


    public static float TotalValueIR, TotalValueUV;

    public static void setData(Response tray, SingleTraySlot singleTraySlot) {
        if (singleTraySlot == null) {
//            DisplayUVValues(tray.getUVSensor001(), tray.getUVSensor002(), 6);//6- hardcoded(conversion rate);
            SetSlotNames("Adapter", "Cloud Scribe");

        } else {
            try {
//                DisplayUVValues(tray.getUVSensor001(), tray.getUVSensor002(), singleTraySlot.getValue().get(0).getHeight());//6- hardcoded(conversion rate);

                poidIR = singleTraySlot.getValue().get(positionOfSlot("IRSensor", singleTraySlot)).getTraySensorId();
                poidIR = singleTraySlot.getValue().get(positionOfSlot("WeightSensor", singleTraySlot)).getTraySensorId();
                SetSlotNames(singleTraySlot.getValue().get(positionOfSlot("IRSensor", singleTraySlot)).getProductName(), singleTraySlot.getValue().get(positionOfSlot("WeightSensor", singleTraySlot)).getProductName());

            } catch (NullPointerException | IndexOutOfBoundsException e) {
                SetSlotNames("Adapter", "Cloud Scribe");
            }
        }

        DisplayIRValues(tray.getIRSensor001(), tray.getIRSensor002(), tray.getIRSensor003(), tray.getIRSensor004(), tray.getIRSensor005(), tray.getIRSensor006(), tray.getIRSensor007(), tray.getIRSensor008(), tray.getIRSensor009(), tray.getIRSensor010());//6- hardcoded(conversion rate);


        DisplayWeight(tray.getWTSensor001());


    }

    private static int positionOfSlot(String sensortype, SingleTraySlot singleTraySlot) {
        for (int i = 0; i < singleTraySlot.getValue().size(); i++) {
            if (singleTraySlot.getValue().get(i).getSensorDeviceType().equals(sensortype)) {
                return i;
            }
        }
        return 0;
    }

    private static void SetSlotNames(String irName1, String wtName1) {
        irName = irName1;
//        uvName = uvName1;
        wtName = wtName1;
    }

    private static void DisplayWeight(double weight) {
        float totalWeight = 6000;

        double weightPercent = weight / totalWeight;
        weightPercent = weightPercent * 100;

        if (weightPercent < 50) {
            weightColourString = "red";
        }

        if(Math.round(weight)< 0 || Math.round(weight) > 6000) {
            weightCountString = "loading..";
        }else {
            weightCountString = Math.round(weight) + "g";
        }
        totalWeightCountString = Math.round(totalWeight) + "g Available";

    }

    public static void DisplayIRValues(int irSensor1, int irSensor2, int irSensor3, int irSensor4, int irSensor5, int irSensor6, int irSensor7, int irSensor8, int irSensor9, int irSensor10) {

        float IRSensor1 = (irSensor1);
        float IRSensor2 = (irSensor2);
        float IRSensor3 = (irSensor3);
        float IRSensor4 = (irSensor4);
        float IRSensor5 = (irSensor5);
        float IRSensor6 = (irSensor6);
        float IRSensor7 = (irSensor7);
        float IRSensor8 = (irSensor8);
        float IRSensor9 = (irSensor9);
        float IRSensor10 = (irSensor10);


        float maxSensorValue = 48;//hard coded

        float Value = irSensorCount(IRSensor1) + irSensorCount(IRSensor2) + irSensorCount(IRSensor3) + irSensorCount(IRSensor4) + irSensorCount(IRSensor5) + irSensorCount(IRSensor6) + irSensorCount(IRSensor7) + irSensorCount(IRSensor8) + irSensorCount(IRSensor9) + irSensorCount(IRSensor10);

        TotalValueIR = 9;


        irCountString = Math.round(Value) + "";
        totalIrCountString = Math.round(TotalValueIR) + " Available";


        float valuePercent = (Value / TotalValueIR);
        valuePercent = valuePercent * 100;

        if (valuePercent < 50) {
            inStoreColourStringIR = "red";
        }


    }

    public static void DisplayUVValues(int uvSensor1, int uvSensor2, int uvConversion) {
        float UVSensor1 = (uvSensor1);
        float UVSensor2 = (uvSensor2);


        float Conversion = (uvConversion);

        float maxSensorValue = 48;//hard coded

        float Value = ((UVSensor1 + UVSensor2) / Conversion);

        TotalValueUV = ((maxSensorValue * 2) / 6);


        uvCountString = Math.round(Value) + "";
        totalUVCountString = Math.round(TotalValueUV) + " Available";


        float valuePercent = (Value / TotalValueUV);
        valuePercent = valuePercent * 100;

        if (valuePercent < 50) {
            inStoreColourStringUV = "red";
        }


    }


    public static int irSensorCount(float ir) {
        if (ir < 1) {
            return 0;
        } else {
            return 1;
        }
    }

    public static List<TrayDetailsData> adapterObject() {
//        detailsdata = new ArrayList<>();
        SingleTrayActivity.trayDetailsList.clear();
        TrayDetailsData current1 = new TrayDetailsData();
        current1.title = irName;
        current1.inStoreCount = irCountString;

        Log.e("TAG", "Setting data" + current1.inStoreCount);

        current1.inStoreTotalCount = totalIrCountString;
        current1.inStoreColour = inStoreColourStringIR;
        current1.warehouseCount = "9 Box";
        current1.warehouseTotalCount = "3 units per box";
        current1.warehouseColour = warehouseColourString;
        current1.poID = poidIR;
        SingleTrayActivity.trayDetailsList.add(current1);


//        TrayDetailsData current3 = new TrayDetailsData();
//        current3.title = uvName;
//        current3.inStoreCount = uvCountString;
//        current3.inStoreTotalCount = totalUVCountString;
//        current3.inStoreColour = inStoreColourStringUV;
//        current3.warehouseCount = "10 Box";
//        current3.warehouseTotalCount = "5 units per box";
//        current3.warehouseColour = warehouseColourStringUV;
//        SingleTrayActivity.trayDetailsList.add(current3);


        TrayDetailsData current2 = new TrayDetailsData();
        current2.title = wtName;
        current2.inStoreCount = weightCountString;

        Log.e("TAG", "Setting data" + current2.inStoreCount);

        current2.inStoreTotalCount = totalWeightCountString;
        current2.inStoreColour = weightColourString;
        current2.warehouseCount = "120 Units";
        current2.warehouseTotalCount = "130g per unit";
        current2.warehouseColour = warehouseColourString;
        current1.poID = poidWT;
        SingleTrayActivity.trayDetailsList.add(current2);


        return SingleTrayActivity.trayDetailsList;
    }

}

