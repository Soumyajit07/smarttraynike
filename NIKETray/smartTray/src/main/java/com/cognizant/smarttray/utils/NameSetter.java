package com.cognizant.smarttray.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;

import com.cognizant.smarttray.R;
import com.cognizant.smarttray.traycustomization.ExpandableListAdapter;
import com.cognizant.smarttray.traycustomization.TrayConfiguration;

/**
 * Created by 452781 on 9/26/2016.
 */
public class NameSetter {


    /**
     * Created by 452781 on 9/22/2016.
     */

    Context context;


    Button btnDismiss, btnAccept;
    ExpandableListAdapter expandableListAdapter;

    String name;

    public NameSetter(ExpandableListAdapter expandableListAdapter, Context context) {

        this.context = context;
        this.expandableListAdapter = expandableListAdapter;

    }


    public void getPopUp(View parent, int maxLimit, final String listParentName, final Context context) {

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popUpView = inflater.inflate(R.layout.setting_tray_name, null, false);
        final PopupWindow popup = new PopupWindow(popUpView, screenWidth,
                WindowManager.LayoutParams.WRAP_CONTENT, true);
        popup.setContentView(popUpView);
        btnDismiss = (Button) popUpView.
                findViewById(R.id.cancelButton);

        btnAccept = (Button) popUpView.findViewById(R.id.okButon);

        popup.showAtLocation(parent, Gravity.CENTER_HORIZONTAL, 10, 10);

        final EditText namepicker = (EditText) popUpView.findViewById(R.id.namePicker);


        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = namepicker.getText().toString();
                System.out.println("@@# nameNEW " + name);

                TrayConfiguration.listDataChild.get(listParentName).get(0).setProductName(name);
                TrayConfiguration.notifychange();


                popup.dismiss();

            }
        });

        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popup.dismiss();
            }
        });

    }



}

