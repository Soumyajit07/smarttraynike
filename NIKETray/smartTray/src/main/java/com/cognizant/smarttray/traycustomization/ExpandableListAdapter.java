package com.cognizant.smarttray.traycustomization;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cognizant.smarttray.R;
import com.cognizant.smarttray.barcode.ScanActivity;
import com.cognizant.smarttray.model.SlotModel;
import com.cognizant.smarttray.utils.NameSetter;
import com.cognizant.smarttray.utils.ThresholdSetter;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    public static HashMap<String, List<SlotModel>> _listDataChild;

    public static TextView enterProductName;
    public static TextView threshold, enterproductnamelabel, thresholdlabel, or;
    LinearLayout slotQrLayout;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<SlotModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;

//        System.out.println("@@## Passed data to adapter " + _listDataChild.get(_listDataHeader.get(0)).get(0).getProductName());
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final SlotModel childModel = (SlotModel) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandablelist_child_item, null);
        }


        enterProductName = (TextView) convertView
                .findViewById(R.id.enterProductName);
        threshold = (TextView) convertView
                .findViewById(R.id.threshold);

        enterproductnamelabel = (TextView) convertView.findViewById(R.id.enterproductnamelabel);
        thresholdlabel = (TextView) convertView.findViewById(R.id.thresholdlabel);
        or = (TextView) convertView.findViewById(R.id.or);
        slotQrLayout = (LinearLayout) convertView.findViewById(R.id.slotQrLayout);


        if (_listDataHeader.get(groupPosition).equals("Tray Inputs")) {
            or.setVisibility(View.GONE);
            slotQrLayout.setVisibility(View.GONE);
            enterproductnamelabel.setText("Enter Tray Name");
            thresholdlabel.setText("Tray Location");
            threshold.setText("Floor one, zone 1");

            slotQrLayout.setOnClickListener(null);
            threshold.setOnClickListener(null);

            enterProductName.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {


                    new NameSetter(ExpandableListAdapter.this, _context).getPopUp(v, 100, _listDataHeader.get(groupPosition), _context);

                }
            });


            //childModel.setProductName(enterProductName.getText().toString());


        } else {


            or.setVisibility(View.VISIBLE);
            slotQrLayout.setVisibility(View.VISIBLE);
            enterproductnamelabel.setText("Enter Product Name");
            thresholdlabel.setText("Slot Threshold");

            enterProductName.setOnClickListener(null);

            slotQrLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent intent = new Intent(_context, ScanActivity.class);


                    intent.putExtra("listParentName", _listDataHeader.get(groupPosition));
                    intent.putExtra("QRcodeNameMAP", (Serializable) childModel.getQRCodeNameMap());

                    _context.startActivity(intent);


                }
            });

            threshold.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    System.out.println("@@## Threshold clicked");
                    new ThresholdSetter(ExpandableListAdapter.this, _context).getPopUp(v, 100, _listDataHeader.get(groupPosition), _context);


                    //notifyDataSetChanged();

                }
            });

            threshold.setText(String.valueOf(childModel.getThreshold()));

        }

        enterProductName.setText(childModel.getProductName());
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        System.out.println("@@## Children Count " + this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size());
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expandablelist_item, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}